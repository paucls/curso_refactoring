describe("BirthDay", function() {

  var birthDay;

  beforeEach(function() {
    birthDay = new BirthDay();
  });

  it("No party for people born in july", function() {   
    expect(birthDay.isThereAPartyReadyForMe('Adrian','19/07/1987')).toEqual('No party for you');
  });

  it("No party for people born in December", function() {   
    expect(birthDay.isThereAPartyReadyForMe('Alma','19/12/1987')).toEqual('No party for you');
  });

  it("Party for people born in May", function() {   
    expect(birthDay.isThereAPartyReadyForMe('Teo','19/05/1987')).toEqual('Party!!!');
  });

  it("Always party for Xavi", function() {   
    expect(birthDay.isThereAPartyReadyForMe('Xavi','19/12/1987')).toEqual('Party!!!');
  });

});

describe("Alphabet", function() {

  var alphabet;

  beforeEach(function() {
    alphabet = new Alphabet();
  });

  it("toCaps of letter 'a' should be 'A'", function() {
    expect(alphabet.giveMeAn('a').toCaps()).toEqual('A');
  });

  it("toLower of letter 'A' should be 'a'", function() {
    expect(alphabet.giveMeAn('A').toLower()).toEqual('a');
  });

  it("toLower of null should be ''", function() {
    expect(alphabet.giveMeAn().toLower()).toEqual('');
  });

});

