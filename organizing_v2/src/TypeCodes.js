function HumanMetabolism() {

	var complexionType = new ComplexionType();
	this.complexion;

	this.getDailyCalories = function() {
		var calories = 0;
		switch(this.getComplexionType()) {
		case complexionType.FATTY:
			calories = 1000;
			break;
		case complexionType.SKINNY:
			calories = 3000;
			break;
		case complexionType.ATHLETIC:
			calories = 2000;
			break;
		case complexionType.MORBID:
			calories = 500;
			break;
		}
		return calories;
	};

	this.getComplexionType = function() {
		return this.complexion.getTypeCode();
	}

	this.setComplexion = function(aComplexion) {
		this.complexion = complexionType.newType(aComplexion);
	};
	this.moreMagic = function() {};
	this.needsMoreFood = function() {};

};

function ComplexionType() {
	this.FATTY = 0;
	this.SKINNY = 1;
	this.ATHLETIC = 2;
	this.MORBID = 3;

	this.newType = function(aCode) {
		switch(aCode) {
		case this.FATTY:
			return new ComplexionFatty();
		case this.SKINNY:
			return new ComplexionSkinny();
		case this.ATHLETIC:
			return new ComplexionAthletic();
		case this.MORBID:
			return new ComplexionMorbid();
		}
		throw "Incorrect Complexion Code";
	};

	ComplexionType.prototype.getTypeCode = function() {
    	throw "You must implement getTypeCode";
	}
};

function ComplexionFatty() {
	this.getTypeCode = function() {
		return this.FATTY;
	};
};
ComplexionFatty.prototype = new ComplexionType();

function ComplexionSkinny() {
	this.getTypeCode = function() {
		return this.SKINNY;
	};
};
ComplexionSkinny.prototype = new ComplexionType();

function ComplexionAthletic() {
	this.getTypeCode = function() {
		return this.ATHLETIC;
	};
};
ComplexionAthletic.prototype = new ComplexionType();

function ComplexionMorbid() {
	this.getTypeCode = function() {
		return this.MORBID;
	};
};
ComplexionMorbid.prototype = new ComplexionType();